//
//  FlickrCollectionViewController.swift
//  FlickrProbe
//
//  Created by Assem Sherief on 2/3/15.
//  Copyright (c) 2015 AssemSherief. All rights reserved.
//

import UIKit

let reuseIdentifier = "ImageCell"
let flickrAPIKey = "afaf1315c5a9c34d2811ae9b1bb38ca3"

class FlickrCollectionViewController: UICollectionViewController, UITextFieldDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var searchTextField: UITextField!
    var searchResults = NSMutableArray()
    var page = 1
    var searchTerm = ""
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    let layoutObject = FlickrCollectionViewFlowLayout()
    var selectedPhoto : NSMutableDictionary?
    
    override func viewDidLoad() {
        
        //assign te custom layout
        collectionView?.collectionViewLayout = layoutObject
        activityIndicator.hidesWhenStopped = true
    }
    
    //MARK: Text Field Delegate Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        //reset everything before loading the new search results
        searchResults.removeAllObjects()
        page = 1
        searchTerm = textField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        selectedPhoto = nil
            
        if (searchTerm != ""){
            collectionView?.reloadData()
            handleSearchResults()
        }
        
        //Dismiss keyboard
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: UICollectionView Data Source
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //only one section with the current search results
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //count the objects in the results array
        return searchResults.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! FlickrCollectionViewCell
        
        //set cell background
        cell.backgroundColor = UIColor.blackColor()
        
        //if a photo is selected to be maximized then:
        if let photo = selectedPhoto{
            if photo == searchResults[indexPath.row] as! NSMutableDictionary{
                
                //clear any already existing image in the cell
                cell.photoImageView.image = nil
                
                //show the activity indicator and start spinning
                cell.cellActivityIndicator.hidden = false
                cell.cellActivityIndicator.startAnimating()
                
                //change the content mode of the image
                cell.photoImageView.contentMode = .ScaleAspectFit
                
                //if the selected photo doesn't have the big photo already loaded then load it directly
                if photo["bigPhoto"] != nil{
                    cell.cellActivityIndicator.stopAnimating()
                    cell.cellActivityIndicator.hidden = true
                    cell.photoImageView.image = photo["bigPhoto"] as? UIImage
                }
                else{
                    
                    //if not then download it then assign in the callback
                    downloadPhotoWithSize(photo, size: "b"){
                        result, error in
                        
                        cell.cellActivityIndicator.stopAnimating()
                        cell.cellActivityIndicator.hidden = true
                        collectionView.scrollEnabled = false
                        
                        if error == nil{
                            cell.photoImageView.image = result
                            self.searchResults.removeObject(photo)
                            photo["bigPhoto"] = result
                            self.searchResults.insertObject(photo, atIndex: indexPath.row)
                        }
                        else{
                            let alert = UIAlertView(title: "Error", message: error?.localizedDescription, delegate: nil, cancelButtonTitle: "ok")
                            alert.show()
                        }
                    }
                }
            }
            return cell
        }
        
        //if no cell is selected then load the thumbnail into the cell image view
        //get the photo from the array
        let photo = searchResults[indexPath.row] as! NSDictionary
        
        //assign the thumbnail to the cell image
        if let thumbnail = photo["thumbnail"] as? UIImage{
            cell.photoImageView.image = thumbnail
            cell.photoImageView.contentMode = .ScaleToFill
        }
        
        return cell
    }
    
    //MARK: UICollectionView Delegate Flow Layout
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    
        //if there is a photo selected then set the size of it's cell to fill the screen
        if let currentPhoto = selectedPhoto{
            if currentPhoto == searchResults[indexPath.row] as! NSDictionary{
                var size = collectionView.bounds.size
                size.height -= topLayoutGuide.length - bottomLayoutGuide.length
                return size
            }
        }
        
        //if no photo is selected then:
        //get the current photo from the results array
        let photo = searchResults[indexPath.row] as! NSDictionary
        
        //if it has a thumbnail use its size, if not use a default size
        let thumbnail = photo["thumbnail"] as? UIImage
        
        if let size = thumbnail?.size{
            let border = CGFloat(10)
            let newSize = CGSize(width: size.width+border, height: size.height + border)
            return newSize
        }
        return CGSize(width: 100, height: 100)
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        //if a photo is already selected then deselect it and re-enable scrolling and enable the load more button
        if !activityIndicator.isAnimating(){
            if let _ = selectedPhoto{
                selectedPhoto = nil
                collectionView.scrollEnabled = true
                navigationItem.rightBarButtonItem?.enabled = true
            }
            else{
                //if no photo is selected then set the one tapped as the selected and disable the load more button
                navigationItem.rightBarButtonItem?.enabled = false
                selectedPhoto = searchResults[indexPath.row] as? NSMutableDictionary
            }
            
            //Reload this specific cell and scroll to it
            collectionView.reloadItemsAtIndexPaths([indexPath])
            collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
        }
    }
    
    //MARK: Flickr Related Methods
    func createFlickrSearchURLForSearchTerm(term: String, page: Int) -> NSURL{
        
        //create the proper search term using percent escapes
        let escapedTerm = term.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        //return the URL that will be used with the API with default 20 images per page
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(flickrAPIKey)&text=\(escapedTerm)&per_page=20&page=\(page)&format=json&nojsoncallback=1"
        return NSURL(string: URLString)!
    }
    
    func searchFlickrUsingSearchTerm(term: String, page: Int, completion : (result: NSDictionary?, error : NSError?)-> Void){

        let processingQueue = NSOperationQueue()
        let searchURL = createFlickrSearchURLForSearchTerm(term, page: page)
        let searchRequest = NSURLRequest(URL: searchURL)
        
        //open a connection and send the search request
        NSURLConnection.sendAsynchronousRequest(searchRequest, queue: processingQueue) {response, data, error in
            if error == nil {
                //convert the JSON result into foundation objects
//                let JSONError : NSError?
                var resultsDictionary : NSDictionary?
                do {
                    resultsDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions(rawValue: 0)) as? NSDictionary
                }
                catch{
                    completion(result: nil,error: error as NSError)
                    return
                }
                
//                if JSONError != nil {
//                }
                
                //check for response issues and handle them
                switch (resultsDictionary!["stat"] as! String) {
                case "ok":
                    print("Results processed OK")
                case "fail":
                    let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:resultsDictionary!["message"]!])
                    completion(result: nil, error: APIError)
                    return
                default:
                    let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Uknown API response"])
                    completion(result: nil, error: APIError)
                    return
                }
                
                //Extract photos data
                if let resultsDict = resultsDictionary{
                    
                    if let photosContainer = resultsDict["photos"] as? NSDictionary{
                        _ = photosContainer["page"] as! Int
                        _ = [NSDictionary]()
                        
                        let searchResults = photosContainer["photo"] as! [NSDictionary]
                        
                        if searchResults.count > 0{
                        
                            for photo in searchResults{
                                let currentPhoto = NSMutableDictionary()
                                
                                let photoID = photo["id"] as? String ?? ""
                                let farm = photo["farm"] as? Int ?? 0
                                let server = photo["server"] as? String ?? ""
                                let secret = photo["secret"] as? String ?? ""
                                
                                currentPhoto["photoID"] = photoID
                                currentPhoto["farm"] = farm
                                currentPhoto["server"] = server
                                currentPhoto["secret"] = secret
                                
                                self.downloadPhotoWithSize(currentPhoto, size: "t"){
                                    photo, error in
                                    
                                    if error == nil{
                                        currentPhoto["thumbnail"] = photo
                                    }
                                }
                                
                                //Return the fetched data to the collection view
                                dispatch_async(dispatch_get_main_queue(), {
                                    completion(result: currentPhoto, error: nil)
                                })
                                
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                //hide the activity indicator and enable the load more button
                                self.activityIndicator.stopAnimating()
                                self.navigationItem.rightBarButtonItem?.enabled = true
                            })
                        }
                        else{
                            //Return the fetched data to the collection view
                            dispatch_async(dispatch_get_main_queue(), {
                                completion(result: nil, error: nil)
                                //hide the activity indicator
                                self.activityIndicator.stopAnimating()
                            })
                        }
                    }
                }
            }
            else{
                completion(result: nil,error: error)
                return
            }
        }
    }
    
    func downloadPhotoWithSize(photo: NSDictionary, size: String, completion : (photo: UIImage?, error: NSError?)-> Void){
        
        //download the image of the given photo with the specific size requested
        
        let processingQueue = NSOperationQueue()
        
        let photoID = photo["photoID"] as! String
        let farm = photo["farm"] as! Int
        let server = photo["server"] as! String
        let secret = photo["secret"] as! String
        
        let imageURL = NSURL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret)_\(size).jpg")!
        
        //if the requested image size is big then do it in the background
        if size == "b"{
            let imageRequest = NSURLRequest(URL: imageURL)
            NSURLConnection.sendAsynchronousRequest(imageRequest, queue: processingQueue) {response, data, error in
                
                if error == nil{
                    dispatch_async(dispatch_get_main_queue(), {
                        completion(photo: UIImage(data: data!), error: nil)
                    })
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), {
                        completion(photo: nil, error: error)
                    })
                }
            }
        }
        else{
            //else if it is a thumbnail then fetch it directly to achieve the image flying and rotation effect
            let imageData = NSData(contentsOfURL: imageURL)
            dispatch_async(dispatch_get_main_queue(), {
                if let _ = imageData {
                    completion(photo: UIImage(data: imageData!), error: nil)
                }
            })
        }
    }
    
    //MARK: Control Methods
    @IBAction func loadMoreImages(sender: AnyObject) {
        
        //to load more images the url is altered to request the second page (the next 20 images)
        page += 1
        handleSearchResults()
    }
    
    func handleSearchResults(){
        
        //Display an activity indicator while the search is performed and the results are loaded
        navigationItem.rightBarButtonItem?.enabled = false
        searchTextField.addSubview(activityIndicator)
        activityIndicator.frame = searchTextField.bounds
        activityIndicator.startAnimating()
        
        //Pass the search term and page number to the search method
        searchFlickrUsingSearchTerm(searchTerm, page: page) {
            result, error in
            
            //if no errors then
            if error == nil && result != nil{
                if result?.count > 0 {
                    self.searchResults.addObject(result!)
                    self.collectionView?.insertItemsAtIndexPaths([NSIndexPath(forItem: self.searchResults.count-1, inSection: 0)])
                    self.collectionView?.scrollToItemAtIndexPath(NSIndexPath(forItem: self.searchResults.count-1, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
                }
            }
            else if result == nil{
                //if no results are found, show a message
                let alert = UIAlertView(title: "No Results", message: "No matching results were found", delegate: nil, cancelButtonTitle: "ok")
                alert.show()
            }
            else{
                //if there is an error, display its description
                let alert = UIAlertView(title: "Error", message: error?.localizedDescription, delegate: nil, cancelButtonTitle: "ok")
                alert.show()
                //hide the activity indicator
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        
        //if the device rotates then if there is a photo selected, reload it to fill the screen again and scroll to it
        if let photo = selectedPhoto{
            let indexPath = NSIndexPath(forItem: searchResults.indexOfObject(photo), inSection: 0)
            collectionView?.reloadItemsAtIndexPaths([indexPath])
            collectionView?.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
        }
    }
}
