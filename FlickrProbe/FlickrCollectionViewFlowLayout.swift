//
//  FlickrCollectionViewFlowLayout.swift
//  FlickrProbe
//
//  Created by Assem Sherief on 2/3/15.
//  Copyright (c) 2015 AssemSherief. All rights reserved.
//

import UIKit
import Darwin

class FlickrCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    var indexPathsToAnimate = NSMutableArray()
    
    override init() {
        super.init()
        self.scrollDirection = UICollectionViewScrollDirection.Horizontal
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForCollectionViewUpdates(updateItems: [UICollectionViewUpdateItem]) {
        super.prepareForCollectionViewUpdates(updateItems)
        let indexPaths = NSMutableArray()
        
        for updateItem in updateItems{
            
            if updateItem.updateAction == UICollectionUpdateAction.Insert{
                indexPaths.addObject(updateItem.indexPathAfterUpdate!)
            }
        }
        
        self.indexPathsToAnimate = indexPaths;
    }
    
    override func finalizeCollectionViewUpdates() {
        super.finalizeCollectionViewUpdates()
        self.indexPathsToAnimate.removeAllObjects()
    }
    
    override func initialLayoutAttributesForAppearingItemAtIndexPath(itemIndexPath: NSIndexPath) -> UICollectionViewLayoutAttributes? {
        
        let attr = layoutAttributesForItemAtIndexPath(itemIndexPath)
        
        if (indexPathsToAnimate.containsObject(itemIndexPath)){
            attr!.transform = CGAffineTransformRotate(CGAffineTransformMakeScale(0.2, 0.2), CGFloat(M_PI));
            if let view = collectionView{
                attr!.center = CGPointMake(CGRectGetMidX(view.bounds), CGRectGetMaxY(view.bounds));
            }
            indexPathsToAnimate.removeObject(itemIndexPath)
        }
        return attr;
    }
}
