//
//  FlickrCollectionViewCell.swift
//  FlickrProbe
//
//  Created by Assem Sherief on 2/3/15.
//  Copyright (c) 2015 AssemSherief. All rights reserved.
//

import UIKit

class FlickrCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var cellActivityIndicator: UIActivityIndicatorView!
}
